PRODUCT_PACKAGES += AgentTools
PRODUCT_PACKAGES += TydomMqttListener

ifeq ($(strip $(DELTADORE_TYDOM_TEST_APK)),yes)
  PRODUCT_PACKAGES += TydomTestApk
endif

# Copy x3d_driver
# Done with patch in kernel-3.18/Android.mk (deltadore-8-vendor_deltadore_v2.patch)
#PRODUCT_COPY_FILES += \
#$(LOCAL_PATH)/modules/x3d_video.ko:system/lib/modules/x3d_video.ko

# Copy deltadore daemon & launcher
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/bin/zipgateway_run.sh:system/bin/zipgateway_run.sh

# Copy deltadore daemon
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/bin/deltadore:system/bin/deltadore

# Copy deltadore tydom (service launcher for deltadore daemon and agenttools)
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/bin/tydom.sh:system/bin/tydom.sh \
$(LOCAL_PATH)/bin/agenttools.sh:system/bin/agenttools.sh

# Copy deltadore STM32 firmwares
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/STM32/P23190011_V01-00-04.hex:data/misc/P23190011.hex \
$(LOCAL_PATH)/STM32/P23200011_V01-00-07.hex:data/misc/P23200011.hex \
$(LOCAL_PATH)/STM32/P23190011_V01-00-04.hex:system/vendor/P23190011.hex \
$(LOCAL_PATH)/STM32/P23200011_V01-00-07.hex:system/vendor/P23200011.hex

# Copy deltadore init.rc
PRODUCT_COPY_FILES += \
$(LOCAL_PATH)/conf/init.deltadore-$(TARGET_BUILD_VARIANT).rc:root/init.deltadore.rc \
$(LOCAL_PATH)/conf/privapp-permisisons-com.deltadore.mobile.smarthome.agenttools.xml:system/etc/permissions/privapp-permisisons-com.deltadore.mobile.smarthome.agenttools.xml \
$(LOCAL_PATH)/conf/privapp-permisisons-com.deltadore.tydommqttinterface.xml:system/etc/permissions/privapp-permisisons-com.deltadore.tydommqttinterface.xml

