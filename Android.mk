LOCAL_PATH := $(call my-dir)

$(info DELTADORE_TYDOM_SUPPORT = $(DELTADORE_TYDOM_SUPPORT))

ifeq ($(strip $(DELTADORE_TYDOM_SUPPORT)), yes)
  $(info DELTADORE_TYDOM_MEDIATION_CONFIG = $(DELTADORE_TYDOM_MEDIATION_CONFIG))
  # => when changed, do:
  # rm -rf out/target/product/br8735m_6h_m1/system/app/AgentTools out/target/product/br8735m_6h_m1/obj/APPS/AgentTools_intermediates
  $(info DELTADORE_TYDOM_TEST_APK = $(DELTADORE_TYDOM_TEST_APK))
  # => when changed, do:
  # rm -rf out/target/product/br8735m_6h_m1/system/app/TydomTestApk out/target/product/br8735m_6h_m1/obj/APPS/TydomTestApk_intermediates

  include $(CLEAR_VARS)
    LOCAL_MODULE := AgentTools
    LOCAL_MODULE_CLASS := APPS
    LOCAL_CERTIFICATE := PRESIGNED
    ifeq ($(strip $(DELTADORE_TYDOM_MEDIATION_CONFIG)),prod)
      LOCAL_SRC_FILES := apk/AgentTools-v0.23-prod.apk
    else
      LOCAL_SRC_FILES := apk/AgentTools-v0.23-pprod.apk
    endif
  include $(BUILD_PREBUILT)

  include $(CLEAR_VARS)
    LOCAL_MODULE := TydomMqttListener
    LOCAL_MODULE_CLASS := APPS
    LOCAL_CERTIFICATE := PRESIGNED
    LOCAL_SRC_FILES := apk/tydom-mqtt-interface-v01.01.02-2.apk
    # DOCUMENTATION: apk/Specs_ListnerWS-v1.1.0.xlsx
  include $(BUILD_PREBUILT)

  ifeq ($(strip $(DELTADORE_TYDOM_TEST_APK)),yes)
    include $(CLEAR_VARS)
      LOCAL_MODULE := TydomTestApk
      LOCAL_MODULE_CLASS := APPS
      LOCAL_CERTIFICATE := platform
      LOCAL_SRC_FILES := apk/test/tydom-archosDebug-02.04.01-4-arkea.apk
    include $(BUILD_PREBUILT)
  endif

endif
