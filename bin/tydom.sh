#!/system/bin/sh

action=$1

start_tydom() {
    echo "Deltadore (c) start Tydom daemon"

    # create folder to store video
    mkdir -p /data/system/deltadore/video
    chown -R system:system /data/system/deltadore/video

    if [ -f /data/system/deltadore/klog ]; then
		echo 7 > /sys/module/x3d_video/parameters/log_level
    else
		# on release, set log_level to 4 (warning)
		echo 6 > /sys/module/x3d_video/parameters/log_level
    fi

    if [ -f /data/system/deltadore/debug ]; then
		# create core folder & configure kernel core generation
		if [ ! -d /data/system/deltadore/core ]; then
			mkdir -p /data/system/deltadore/core
		fi
		
		# Core file name:
		# - %e: executable file name
		# - %p: pid
		# - %t: time
		# - %s: number of the signal causing dump
    	echo "/data/system/deltadore/core/core.%p.signal_%s.%e" > /proc/sys/kernel/core_pattern
    	
		# activate core generation
    	ulimit -c unlimited
    else
    	# on release, set log_level to 4 (warning)
    	echo 6 > /sys/module/x3d_video/parameters/log_level
    	rm -rf /data/system/deltadore/core
    fi

    if [ -f /data/system/deltadore/infra ]; then
		infra=`cat /data/system/deltadore/infra`
		if [ "$infra" == "1" ]; then
		    echo "Force preprod config (requires /data/system/deltadore/deltadore-infra1.conf)"
		    cp -f /data/system/deltadore/deltadore-infra1.conf /data/system/deltadore/deltadore.conf
		fi
		if [ "$infra" == "0" ]; then
		    echo "Force prod config (requires /data/system/deltadore/deltadore-infra0.conf)"
		    cp -f /data/system/deltadore/deltadore-infra0.conf /data/system/deltadore/deltadore.conf
		fi
	fi

	timestamp=`date +%Y%m%d-%H%M%S`
	echo $timestamp": Service Tydom starting" >> /data/local/tmp/tydom.log

	chmod 400 /data/local/tmp/tydom.log
	chown system:system /data/local/tmp/tydom.log

	if [ -f /data/local/tmp/test_expect ]; then
	# Expect test detected, do not log
		rm -f /data/local/tmp/log2file 
	else
		# Activate Tydom logs
		touch /data/local/tmp/log2file

		chmod 400 /data/local/tmp/log2file
		chown system:system /data/local/tmp/log2file
	fi

	# start deltadore tydroid deamon
	deltadore
}

show_threads() {
	pid=$1
	if [ -f /data/system/deltadore/debug ]; then
		ps -c -t -p  $pid | busybox awk -F" " '{print "\t"$2"\t"$6"\t"$7"\t"$8"\t"$9"\t"$10"\t"$14" "$15" "$16}'
	fi
}

purge_dmesg() {
	if [ -f /data/system/deltadore/debug ]; then
		dmesg -c > /dev/null
	fi
}
show_dmesg() {
	if [ -f /data/system/deltadore/debug ]; then
		dmesg -c | grep "init:"
	fi
}

check_deadlock () {
	pid=$1
	echo -n "Check if locked"
	for i in 1 2 3 4 5 6 7 8 9 10; do
		wchan=`ps -p $pid | grep deltadore | busybox awk -F" " '{print $10}'`
		# Deltadore is not suppose to sleep in futex but in hrtimer_na
#		if [ "$wchan" == "hrtimer_na" ]; then
		if [ "$wchan" == "futex_wait" ]; then
			echo -n ".$i"
			sleep 1
		else
			echo ", sleeping in "$wchan" => not locked"
			return
		fi
	done

	if [ -f /data/system/deltadore/debug_futex ]; then
		echo "debug mode, check which process is locked in futex wait:"
		ps -c -t -p  $pid | busybox awk -F" " '{print "\tstrace -p "$2"\t\ttask: "$14" "$15" "$16"\t\tstate: "$11}'
		return
	fi

	echo " locked for "$i" seconds, /!\\ shoot Tydom daemon /!\\"

	timestamp=`date +%Y%m%d-%H%M%S`

	sleep 1
	dmesg -c > /dev/null
	echo $timestamp": Service Tydom is locked in futex_wait, shoot it" >> /data/local/tmp/tydom.log

	kill -9 $pid

	sleep 1

	if [ -f /data/local/tmp/log2file ]; then
		dmesg -c | grep "init:" > /data/local/tmp/logs/$timestamp.shoot
	else
		if [ -f /data/system/deltadore/log2file ]; then
			dmesg -c | grep "init:" > /data/system/deltadore/logs/$timestamp.shoot
		else
			dmesg -c | grep "init:"
		fi
	fi
}

kill_tydom() {
	pid=$1
	ppid=`ps -p $pid | grep deltadore | busybox awk -F" " '{print $3}'`
	pidname=`cat /proc/$pid/cmdline`
	ppidname=`cat /proc/$ppid/cmdline`

	if [ -f /data/system/deltadore/exit_status.log ]; then	
		rm /data/system/deltadore/exit_status.log
	fi

	# purge dmesg
	purge_dmesg
	
	echo "Deltadore (c) stop Tydom daemon, pid "$pid" ("$pidname"), started by "$ppid" ("$ppidname")"
	
	# show threads
	show_threads $pid
	
	echo -n "Stop signals"
	
	# Send signal every second up to 15 seconds
	for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30; do
		if [ -f /proc/$pid/cmdline ]; then
			kill -s 2 $pid
			sleep 1
			echo -n ".$i"
		else
			echo ""
			break
		fi
	done

	if [ -f /proc/$pid/cmdline ]; then
		echo " /!\ cannot stop nicely deltadore $pid"
		show_dmesg
		echo "-> shoot it"
		stop tydom
		# Kill it anyway, in case it was not managed by the system service
		kill -9 $pid
		timestamp=`date +%Y%m%d-%H%M%S`
		echo $timestamp": Service Tydom forced to stop" >> /data/local/tmp/tydom.log

		usleep 500000
		rm -f /data/system/deltadore/dd_tydomux1.pid
		show_dmesg
	else
		timestamp=`date +%Y%m%d-%H%M%S`
		echo $timestamp": Service Tydom stopped" >> /data/local/tmp/tydom.log

		if [ -f /data/system/deltadore/dd_tydomux1.pid ]; then
			echo " /!\ warning, stop process was not completed"
			show_dmesg
			echo "-> remove pid file"
			rm -f /data/system/deltadore/dd_tydomux1.pid
			show_dmesg
		else
			echo "stopped"
			if [ -f /data/system/deltadore/exit_status.log ]; then
				cat /data/system/deltadore/exit_status.log
			fi
			show_dmesg
		fi
	fi
	
	# Note:
	# if dmesg show "Service 'tydom' (pid xxxxx) exited with status 137"
	# => run out of memory ?
	
}

stop_tydom() {
	# stop current execution of deltadore
	if [ -f /data/system/deltadore/dd_tydomux1.pid ]; then
		pid=`cat /data/system/deltadore/dd_tydomux1.pid`
		pidname=`cat /proc/$pid/cmdline`
		if [ "$pidname" == "deltadore" ]; then
			kill_tydom $pid
		else
			echo "Deltadore (c) Tydom daemon is not running, remove old pid file"
			rm -f /data/system/deltadore/dd_tydomux1.pid
		fi
	else
		pids=`ps | grep deltadore |  busybox awk -F" " '{print $2}'`
		deamon_detected=0
		for pid in $pids
		do
			pidname=`cat /proc/$pid/cmdline`
			if [ "$pidname" == "deltadore" ]; then
				echo "Deltadore (c) Tydom daemon is running, pid "$pid", but without pid file"
				kill_tydom $pid
				deamon_detected=1
			fi
		done
		if [ "$deamon_detected" -eq "0" ]; then
			echo "Deltadore (c) Tydom daemon is not running"
		fi
	fi
}

status_tydom() {
	# stop current execution of deltadore
	if [ -f /data/system/deltadore/dd_tydomux1.pid ]; then
		pid=`cat /data/system/deltadore/dd_tydomux1.pid`
		pidname=`cat /proc/$pid/cmdline`
		if [ "$pidname" == "deltadore" ]; then
			echo "Deltadore (c) Tydom daemon is running, pid "$pid
			show_threads $pid
		else
			echo "Deltadore (c) Tydom daemon is not running, but pid file ("$pid") is present"
		fi
	else
		pids=`ps | grep deltadore |  busybox awk -F" " '{print $2}'`
		deamon_detected=0
		for pid in $pids
		do
			pidname=`cat /proc/$pid/cmdline`
			if [ "$pidname" == "deltadore" ]; then
				echo "Deltadore (c) Tydom daemon is running, pid "$pid", but without pid file"
				show_threads $pid
				deamon_detected=1
			fi
		done
		if [ "$deamon_detected" -eq "0" ]; then
			if [ -f /data/system/deltadore/deltadore.conf ]; then
				echo "Deltadore (c) Tydom daemon is not running"
			else
				echo "Deltadore (c) Tydom daemon has no configuration yet"
			fi
		fi
	fi

	# List application using driver x3d_video
	x3dv_users=`lsof /dev/tty | grep -c X3DV`
	if [ "$x3dv_users" -eq "0" ]; then
		echo "x3d_video driver is not used"
	else
		echo "x3d_video driver is used by:"
		lsof /dev | grep X3DV | busybox awk -F" " '{print $1}'
	fi
	if [ -f /data/system/deltadore/klog ]; then
		echo "x3d_video driver is in logging mode"
	fi
	
	if [ -f /data/local/tmp/log2file ]; then
		# Valid only if compiled with CONFIG_TMP_PREFIX=/data/local/tmp
		echo "Tydom daemon is logging in /data/local/tmp/logs"
	fi
	if [ -f /data/system/deltadore/debug ]; then
		echo "Tydom daemon is in debug mode with core dump"
	fi
}

alive_tydom() {
	# stop current execution of deltadore
	if [ -f /data/system/deltadore/dd_tydomux1.pid ]; then
		pid=`cat /data/system/deltadore/dd_tydomux1.pid`
		pidname=`cat /proc/$pid/cmdline`
		if [ "$pidname" == "deltadore" ]; then
			echo "Deltadore (c) Tydom daemon is running, pid "$pid
			check_deadlock $pid
		else
			echo "Deltadore (c) Tydom daemon is not running"
		fi
	else
		pids=`ps | grep deltadore |  busybox awk -F" " '{print $2}'`
		deamon_detected=0
		for pid in $pids
		do
			pidname=`cat /proc/$pid/cmdline`
			if [ "$pidname" == "deltadore" ]; then
				echo "Deltadore (c) Tydom daemon is running, pid "$pid", but without pid file"
				check_deadlock $pid
				deamon_detected=1
			fi
		done
		if [ "$deamon_detected" -eq "0" ]; then
			if [ -f /data/system/deltadore/deltadore.conf ]; then
				echo "Deltadore (c) Tydom daemon is not running"
			else
				echo "Deltadore (c) Tydom daemon has no configuration yet"
			fi
		fi
	fi
}

track_tydom() {
	while [ 1 ]; do
		sleep 600
		tydom.sh alive
	done
}

usage() {
  echo "Usage: "$0" {start|stop|restart|status|archlogs|alive|track}"
}

archive_dir() {
  dir=$1
  cd $dir
  timestamp=`date +%Y%m%d-%H%M%S`
  echo $timestamp": Service Tydom archiving logs in /data/local/tmp/tydom_logs.tar.gz" >> /data/local/tmp/tydom.log
  cp /data/local/tmp/tydom.log    .
  tar -zcvf /data/local/tmp/tydom_logs.tar.gz *.log > /dev/null
  chmod 400 /data/local/tmp/tydom_logs.tar.gz
  chown system:system /data/local/tmp/tydom_logs.tar.gz
  rm -rf *
  cd -
}

archives_logs() {
	if [ -f /data/local/tmp/log2file ]; then
		# Valid only if compiled with CONFIG_TMP_PREFIX=/data/local/tmp
		archive_dir /data/local/tmp/logs
	fi
  
}

# activate core generation (valid only for this process & child, not for the caller)
ulimit -c unlimited

case "$action" in
  start)
    start_tydom
    ;;
  stop)
    stop_tydom
    ;;
  restart)
    stop_tydom
    start_tydom
    ;;
  status)
    status_tydom
    ;;
  archlogs)
    archives_logs
    ;;
  alive)
    alive_tydom
    ;;
  track)
    track_tydom
    ;;
  "")
    status_tydom
    ;;
  *)
    usage
    ;;
esac

